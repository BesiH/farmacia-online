package com.farmacia.Farmacia.Online.model;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;

@Entity
@Table(name= "bill")
public class Bill {
    @Id
    private Long id;
    @Column(name ="order_id")
    private Long orderId;
    @Column(name ="client_id")
    private Long clientId;
    @Column(name ="employee_id")
    private Long employeeId;


    @ElementCollection
    @CollectionTable(name = "bill_products", joinColumns = @JoinColumn(name = "bill_id"))
    @Column(name = "product_id")
    private List<Long> product;

    @Column(name = "value")
    private BigDecimal value;

    @Column(name = "status")
    private String status;

    public Bill() {
    }

    public Bill(Long orderId, Long clientId, Long employeeId, List<Long> product, BigDecimal value, String status) {
        this.orderId = orderId;
        this.clientId = clientId;
        this.employeeId = employeeId;
        this.product = product;
        this.value = value;
        this.status = status;
    }

    public Long getId() {
        return id;
    }

    public Long getOrderId() {
        return orderId;
    }

    public Long getClientId() {
        return clientId;
    }

    public Long getEmployeeId() {
        return employeeId;
    }

    public List<Long> getProduct() {
        return product;
    }

    public BigDecimal getValue() {
        return value;
    }

    public String getStatus() {
        return status;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public void setClientId(Long clientId) {
        this.clientId = clientId;
    }

    public void setEmployeeId(Long employeeId) {
        this.employeeId = employeeId;
    }

    public void setProduct(List<Long> product) {
        this.product = product;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}

