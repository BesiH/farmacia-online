package com.farmacia.Farmacia.Online.service;
import com.farmacia.Farmacia.Online.model.Client;
import com.farmacia.Farmacia.Online.repository.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ClientService {

    private final ClientRepository clientRepository;

    @Autowired
    public ClientService(ClientRepository clientRepository) {
        this.clientRepository = clientRepository;
    }

    public List<Client> getAllClients() {
        return clientRepository.findAll();
    }
    public Client getClientById(Long id) {
        return (Client) clientRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("Client not found with id: " + id));
    }

    public Client createClient(Client client) {
        return clientRepository.save(client);
    }

    public Client updateClient(Long id, Client client) {
        Client existingClient = (Client) clientRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("Client not found with id: " + id));

        existingClient.setName(client.getName());
        existingClient.setEmail(client.getEmail());

        return clientRepository.save(existingClient);
    }
    public void deleteClient(Long id) {

        clientRepository.deleteById(id);
    }
}
