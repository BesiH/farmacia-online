package com.farmacia.Farmacia.Online.service;

import com.farmacia.Farmacia.Online.model.User;
import com.farmacia.Farmacia.Online.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {
    private final UserRepository userRepository;

    @Autowired
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public User createUser(User user) {

        return userRepository.save(user);
    }

    public User getUserById(Long id) {
        return userRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("User not found with id: " + id));
    }

    public User getUserByUsername(String username) {

        return userRepository.findByUsername(username);
    }

    public void deleteUser(Long id){

        userRepository.deleteById(id);
    }
}
