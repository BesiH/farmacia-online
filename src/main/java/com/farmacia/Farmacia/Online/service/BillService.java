package com.farmacia.Farmacia.Online.service;

import com.farmacia.Farmacia.Online.model.Bill;
import com.farmacia.Farmacia.Online.repository.BillRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BillService {

    private final BillRepository billRepository;

    @Autowired
    public BillService(BillRepository billRepository) {
        this.billRepository = billRepository;
    }

    public Bill createBill(Bill bill) {
        return billRepository.save(bill);
    }

    public Bill getBillById(Long id) {
        return billRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("Bill not found with id: " + id));
    }

    public Bill updateBill(Long id, Bill updatedBill) {
        Bill bill = getBillById(id);
        bill.setOrderId(updatedBill.getOrderId());
        bill.setEmployeeId(updatedBill.getEmployeeId());
        bill.setClientId(updatedBill.getClientId());
        bill.setProduct(updatedBill.getProduct());
        bill.setValue(updatedBill.getValue());
        bill.setStatus(updatedBill.getStatus());
        return billRepository.save(bill);
    }

    public void deleteBill(Long id) {
        Bill bill = getBillById(id);
        billRepository.delete(bill);
    }

}

