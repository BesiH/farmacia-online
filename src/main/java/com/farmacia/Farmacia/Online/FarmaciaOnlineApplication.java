package com.farmacia.Farmacia.Online;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FarmaciaOnlineApplication {

	public static void main(String[] args) {
		SpringApplication.run(FarmaciaOnlineApplication.class, args);
	}

}
